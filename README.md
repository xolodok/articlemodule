Article module for yii2
===============
Article module

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist xolodok/yii2-article "*"
```

or add

```
"xolodok/yii2-file-article": "*"
```

to the require section of your `composer.json` file.
