<?php

namespace common\traits;

use Yii;
use yii\helpers\FileHelper;
use yii\imagine\Image;

trait ImageUpload
{
    use FileUpload;

    protected $uploadDirs = [
        'original' => 'original',
        'detail' => 'detail',
        'thumb' => 'thumbnail',
    ];

    /**
     * Создание миниатюры изображения
     * @param UploadedFile $file
     * @param string $dest Путь для сохранения файла
     * @param int $width Ширина миниатюры
     * @param int $height Высота миниатюры
     * @param int $quality Качество
     * @return void
     */
    public function thumbnail($file, $dest, $width, $height = null, $quality = 90)
    {
        $img = Image::getImagine()->open($file);
        if(is_null($height)){
            $heigth = $this->getRatioHeight($img, $width);
        }
        $fileName = basename($file, PATHINFO_BASENAME);
        FileHelper::createDirectory($dest);

        Image::thumbnail($file, $width, $height)
            ->save($dest . DIRECTORY_SEPARATOR . $fileName, ['quality' => $quality]);
    }

    /**
     * Создание оптимизированного изображения
     * @param object $file
     * @param string $dest Путь для сохранения файла
     * @param int $quality Качество
     * @return void
     */
    public function optimize($file, $dest, $quality = 90)
    {
        $img = Image::getImagine()->open($file);
        $fileName = basename($file, PATHINFO_BASENAME);
        FileHelper::createDirectory($dest);

        $img->save($dest . DIRECTORY_SEPARATOR . $fileName, ['quality' => $quality]);
    }

    /**
     * Формирование пропорциальной высоты изображения
     * @param object $image
     * @param int $width Ширина изображения
     * @return int
     */
    protected function getRatioHeight($image, $width)
    {
        $size = $image->getSize();
        $ratio = $size->getWidth() / $size->getHeight();

        return round($width / $ratio);
    }

    /**
     * Возвращает папки для загрузки изображений
     * @param boolean $absolute Абсолютный путь
     * @return array
     */
    protected function getFolders($absolute = true)
    {
        $dir = $this->getUploadFolder();
        $dirs = $this->uploadDirs;

        if($absolute){
            array_walk($dirs, function(&$item) use($dir){
                $item = Yii::getAlias('@frontend/web' . $dir . '/' .$item );
            });
        }

        return $dirs;
    }

    public function getPath($type, $attribute = null)
    {
        $attr = is_null($attribute) ? $this->imageAttr : $attribute;

        return $this->getUploadFolder() . '/' . $this->getFolders(false)[$type] . '/' . $this->{$attr};
    }

    public function getSrc($type, $attribute)
    {
        return $this->getPath($type, $attribute);
    }

    public function getOriginalImage()
    {
        return $this->getPath('original');
    }

    public function getThumb()
    {
        return $this->getPath('thumb');
    }

    public function getDetailImage()
    {
        return $this->getPath('detail');
    }

    /**
     * Удаление старого изображения
     * @return void
     */
    public function deleteOldImage($attribute = null)
    {
        $attr = is_null($attribute) ? $this->imageAttr : $attribute;

        foreach ($this->uploadDirs as $dir){
            if(is_file($file = $dir . DIRECTORY_SEPARATOR . $this->{$attr})){
                unlink($file);
            }
        }
    }
}