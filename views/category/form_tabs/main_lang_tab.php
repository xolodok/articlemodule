<?php

if(\xolodok\article\Article::getInstance()->defaultLanguage == $lang)
{
    echo $form->field($model, 'title')->textInput(['maxlength' => true]);
}
else{
    echo $form->field($model, 'title_' . $lang)->textInput(['maxlength' => true])->label($model->getAttributeLabel('title'));
}
?>