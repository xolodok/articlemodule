<?php

use kartik\widgets\SwitchInput;
use kartik\file\FileInput;
use xolodok\article\Article;

echo $form->field($model, 'active')->widget(SwitchInput::className());

echo $form->field($model, 'parent_id')->dropDownList($categories, ['prompt' => Article::t('article', 'Select parent category')]);

if(Article::getInstance()->visibleUrl){
    echo $form->field($model, 'slug')->textInput(['maxlength' => true]);
}

echo $form->field($model, 'image')->widget(FileInput::classname(), [
    'options' => [
        'accept' => 'image/*',
    ],
    'pluginOptions' => [
        'showUpload' => false,
        'showRemove' => $model->isNewRecord,
        'initialPreview' => $model->previewSrc,
        'initialPreviewAsData' => true,
        'initialPreviewShowDelete' => false,
    ]
]);

echo $form->field($model, 'image')->hiddenInput()->label(false);
