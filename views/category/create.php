<?php

use xolodok\article\Article;


/* @var $this yii\web\View */
/* @var $model xolodok\article\models\Category */

$this->title = Article::t('article', 'Create category');
$this->params['breadcrumbs'][] = ['label' => Article::t('article', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
    ]) ?>

</div>
