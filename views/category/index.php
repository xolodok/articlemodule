<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use xolodok\article\Article;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel xolodok\article\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Article::t('article', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
\xolodok\status\AssetBundle::register($this);
?>
<div class="category-index">

    <div class="row">
        <div class="col-md-12">
            <div class="pull-left">
                <a href="<?= Url::to(['create']) ?>" data-toggle="tooltip" title="" class="btn btn-app" ,
                   data-original-title="<?= Article::t('article', 'Create') ?>">
                    <i class="fa fa-plus"></i> <?= Article::t('article', 'Create') ?>
                </a>
            </div>
        </div>
    </div>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'bordered' => true,
        'hover' => true,
        'responsive' => true,
        'panel' => [
            'type' => GridView::TYPE_INFO,
            'heading' => $this->title,
        ],
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-centered'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
//            'parent_id',
//            'meta_title',
//            'meta_description',
            // 'meta_keywords',
            [
                'attribute' => 'active',
                'filter' => Html::activeDropDownList($searchModel, 'active',
                    [Article::t('article', 'Not active'), Article::t('article', 'Active')],
                    ['class' => 'form-control', 'prompt' => Article::t('article', 'Select status')]),
                'format' => 'raw',
                'value' => function ($model) {
                    return \xolodok\status\widgets\StatusButton::widget([
                        'modelId' => $model->id,
                        'value' => $model->active,
                        'statusMessages' => [
                            Article::t('article', 'Not active'),
                            Article::t('article', 'Active')
                        ]
                    ]);
                },
            ],
            // 'slug',
            // 'image',
            // 'date_created',
            // 'date_modified',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Article::t('article', 'Action'),
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', $url, [
                            'class' => 'btn btn-primary',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Article::t('article', 'Edit')
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', $url, [
                            'class' => 'btn btn-danger',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Article::t('article', 'Delete'),
                            'data-method' => 'post',
                            'data-confirm' => Article::t('article', 'Are you sure you want to delete this item?'),
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
