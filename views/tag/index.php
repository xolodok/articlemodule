<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use xolodok\article\Article;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel xolodok\article\models\TagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Article::t('article', 'Tags');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-index">

    <div class="row">
        <div class="col-md-12">
            <div class="pull-left">
                <a href="<?= Url::to(['create'])?>" data-toggle="tooltip" title="" class="btn btn-app", data-original-title="<?= Article::t('article', 'Create')?>">
                    <i class="fa fa-plus"></i> <?= Article::t('article', 'Create')?>
                </a>
            </div>
        </div>
    </div>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'bordered' => true,
        'hover' => true,
        'responsive'=>true,
        'panel'=>[
            'type' => GridView::TYPE_INFO,
            'heading' => $this->title,
        ],
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-centered'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'title',
//            'meta_title',
//            'meta_description',
//            'meta_keywords',
            // 'slug',
            // 'date_created',
            // 'date_modified',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Article::t('article', 'Action'),
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', $url, [
                            'class' => 'btn btn-primary',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Article::t('article', 'Edit')
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', $url, [
                            'class' => 'btn btn-danger',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Article::t('article', 'Delete'),
                            'data-method' => 'post',
                            'data-confirm' => Article::t('article', 'Are you sure you want to delete this item?'),
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
