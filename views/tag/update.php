<?php

use xolodok\article\Article;

/* @var $this yii\web\View */
/* @var $model xolodok\article\models\Tag */

$this->title = Article::t('article', 'Update {modelClass}: ', [
    'modelClass' => 'Tag',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Article::t('article', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Article::t('article', 'Update');
?>
<div class="tag-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
