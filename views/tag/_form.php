<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use xolodok\article\Article;
use yii\bootstrap\Tabs;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\modules\article\models\Tag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tag-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-left">
                <?= Html::submitButton('<i class="fa fa-save"></i>' . Article::t('article', 'Save'), [
                    'class' => 'btn btn-app',
                    'data-toggle' => 'tooltip',
                    'data-original-title' => Article::t('article', 'Save'),
                ])?>

                <a href="<?= Url::to(['/article/tag'])?>" data-toggle="tooltip" title="" class="btn btn-app", data-original-title="<?= Article::t('article', 'Cancel')?>">
                    <i class="fa fa-reply"></i> <?= Article::t('article', 'Cancel')?>
                </a>
            </div>
        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            <?= $model->isNewRecord ? Article::t('article', 'Create') : Article::t('article', 'Editing')?>
        </div>
        <div class="panel-body">
            <div class="post-form">

                <?php
                echo Tabs::widget([
                    'items' => [
                        [
                            'label' => Article::t('article', 'Main information'),
                            'content' => $this->render('form_tabs/main', ['form' => $form, 'model' => $model]),
                            'active' => true
                        ],
                        [
                            'label' => Article::t('article', 'SEO'),
                            'content' => $this->render('form_tabs/seo', ['form' => $form, 'model' => $model]),
                        ],
                    ],
                ]);
                ?>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>