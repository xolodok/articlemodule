<?php

use xolodok\article\Article;

/* @var $this yii\web\View */
/* @var $model xolodok\article\models\Tag */

$this->title = Article::t('article', 'Create tag');
$this->params['breadcrumbs'][] = ['label' => Article::t('article', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
