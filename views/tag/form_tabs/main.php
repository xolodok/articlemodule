<?php

use yii\bootstrap\Tabs;
use xolodok\article\Article;

$langTabs = [];
$active = true;
foreach(Article::getInstance()->languages as $name => $language){
    $langTabs[] = [
        'label' => $language,
        'content' => $this->render("main_lang_tab", ['form' => $form, 'model' => $model, 'lang' => $name]),
        'active' => $active,
    ];
    $active = false;
}

echo Tabs::widget(['items' => $langTabs]);
?>