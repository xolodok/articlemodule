<?php

use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use lajax\translatemanager\models\Language;

$languages = array_reverse(ArrayHelper::map(Language::getLanguages(true), 'language', 'name'));
$langCode = substr(Yii::$app->language, 0, 2);

$langTabs = [];
$active = true;
foreach($languages as $name => $language){
    $langTabs[] = [
        'label' => $language,
        'content' => $this->render("seo_lang_tab", ['form' => $form, 'model' => $model, 'lang' => $name]),
        'active' => $active,
    ];
    $active = false;
}

echo Tabs::widget(['items' => $langTabs]);
?>