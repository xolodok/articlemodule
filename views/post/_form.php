<?php

use xolodok\article\Article;
use yii\bootstrap\Tabs;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\article\models\Post */
/* @var $form yii\widgets\ActiveForm */

?>
<?php $form = ActiveForm::begin([]); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="pull-left">
                <?= Html::submitButton('<i class="fa fa-save"></i>' . Article::t('article', 'Save'), [
                    'class' => 'btn btn-app',
                    'data-toggle' => 'tooltip',
                    'data-original-title' => Article::t('article', 'Save'),
                ])?>

                <a href="<?= Url::to(['/article/post'])?>" data-toggle="tooltip" title="" class="btn btn-app", data-original-title="<?= Article::t('article', 'Cancel')?>">
                    <i class="fa fa-reply"></i> <?= Article::t('article', 'Cancel')?>
                </a>
            </div>
        </div>
    </div>

<div class="panel panel-info">
    <div class="panel-heading">
        <?= $model->isNewRecord ? Article::t('article', 'Create') : Article::t('article', 'Editing')?>
    </div>
    <div class="panel-body">
        <div class="post-form">


        <?php
            echo Tabs::widget([
                'items' => [
                    [
                        'label' => Article::t('article', 'Main information'),
                        'content' => $this->render('form_tabs/main', ['form' => $form, 'model' => $model]),
                        'active' => true
                    ],
                    [
                        'label' => Article::t('article', 'Data'),
                        'content' => $this->render('form_tabs/data', [
                            'form' => $form,
                            'model' => $model,
                            'categories' => $categories,
                            'tags' => $tags,
                        ]),
                    ],
                    [
                        'label' => Article::t('article', 'SEO'),
                        'content' => $this->render('form_tabs/seo', ['form' => $form, 'model' => $model]),
                    ],
                    [
                        'label' => Article::t('article', 'Images'),
                        'content' => $this->render('form_tabs/images', ['model' => $model]),
                    ],
                ],
            ]);
        ?>


        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>