<?php

if(\xolodok\article\Article::getInstance()->defaultLanguage == $lang)
{
    echo  $form->field($model, 'meta_title')->textarea(['rows' => 2]);

    echo $form->field($model, 'meta_description')->textarea(['rows' => 2]);

    echo $form->field($model, 'meta_keywords')->textarea(['rows' => 2]);
}
else{
    echo $form->field($model, 'meta_title_' . $lang)->textarea(['rows' => 2])->label($model->getAttributeLabel('meta_title'));

    echo $form->field($model, 'meta_description_' . $lang)->textarea(['rows' => 2])->label($model->getAttributeLabel('meta_description'));

    echo $form->field($model, 'meta_keywords_' . $lang)->textarea(['rows' => 2])->label($model->getAttributeLabel('meta_keywords'));
}
?>