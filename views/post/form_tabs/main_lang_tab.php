<?php

use dosamigos\tinymce\TinyMce;

if(\xolodok\article\Article::getInstance()->defaultLanguage == $lang)
{
    echo $form->field($model, 'title')->textInput(['maxlength' => true]);

    echo $form->field($model, 'text')->widget(TinyMce::className());
}
else{
    echo $form->field($model, 'title_' . $lang)->textInput(['maxlength' => true])->label($model->getAttributeLabel('title'));

    echo $form->field($model, 'text_' . $lang)->widget(TinyMce::className())->label($model->getAttributeLabel('text'));
}
?>