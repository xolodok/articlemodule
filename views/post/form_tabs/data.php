<?php

use kartik\widgets\SwitchInput;
use kartik\select2\Select2;
use kartik\file\FileInput;
use xolodok\article\Article;

?>

<?= $form->field($model, 'active')->widget(SwitchInput::className()) ?>

<?= $form->field($model, 'category_id')->dropDownList($categories, ['prompt' => Article::t('article', 'Select main category')]) ?>

    <div class="form-group">

        <label class="control-label"><?= Article::t('article', 'Additional categories') ?></label>
        <?= Select2::widget([
            'name' => 'categories',
            'value' => $model->selectedCategories, // initial value
            'data' => $categories,
            'maintainOrder' => true,
            'toggleAllSettings' => [
                'selectLabel' => '<i class="glyphicon glyphicon-ok-circle"></i> Tag All',
                'unselectLabel' => '<i class="glyphicon glyphicon-remove-circle"></i> Untag All',
                'selectOptions' => ['class' => 'text-success'],
                'unselectOptions' => ['class' => 'text-danger'],
            ],
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 10,

            ],
        ]); ?>

    </div>

    <div class="form-group">

        <label class="control-label"><?= Article::t('article', 'Tags') ?></label>
        <?= Select2::widget([
            'name' => 'tags',
            'value' => $model->selectedTags, // initial value
            'data' => $tags,
            'maintainOrder' => true,
            'toggleAllSettings' => [
                'selectLabel' => '<i class="glyphicon glyphicon-ok-circle"></i> Tag All',
                'unselectLabel' => '<i class="glyphicon glyphicon-remove-circle"></i> Untag All',
                'selectOptions' => ['class' => 'text-success'],
                'unselectOptions' => ['class' => 'text-danger'],
            ],
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 10,

            ],
        ]); ?>

    </div>

<?= $form->field($model, 'image')->widget(FileInput::classname(), [
    'options' => [
        'accept' => 'image/*',
    ],
    'pluginOptions' => [
        'showUpload' => false,
        'showRemove' => $model->isNewRecord,
        'initialPreview' => $model->previewSrc,
        'initialPreviewAsData' => true,
        'initialPreviewShowDelete' => false,
    ]
]) ?>

<?= $form->field($model, 'image')->hiddenInput()->label(false) ?>

<?php
if(Article::getInstance()->visibleUrl){
    echo $form->field($model, 'slug')->textInput(['maxlength' => true]);
}
