<?php

use kartik\widgets\FileInput;
?>

<div class="form-group">

    <?= FileInput::widget([
        'name' => 'images[]',
        'language' => Yii::$app->language,
        'options'=>[
            'multiple' => true,
            'accept' => 'image/*'
        ],
        'pluginOptions' => [
            'overwriteInitial' => true,
            'showRemove' => true,
            'showUpload' => false,
            'initialPreview' => $model->previewImages,
            'initialPreviewConfig' => $model->initialPreviewConfig(),
            'initialPreviewAsData' => true,
            'overwriteInitial' => false,
            'initialPreviewShowDelete' => true,
            'showPreview' => true,
            'showRemove' => true,
        ],
    ])?>
</div>

