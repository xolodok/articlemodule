<?php

use xolodok\article\Article;

/* @var $this yii\web\View */
/* @var $model common\modules\article\models\Post */

$this->title = Article::t('article', 'Update {modelClass}: ', [
    'modelClass' => Article::t('article', 'Post'),
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Article::t('article', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Article::t('article', 'Update');
?>
<div class="post-update">

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
        'tags' => $tags,
    ]) ?>

</div>
