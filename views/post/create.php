<?php

use xolodok\article\Article;

/* @var $this yii\web\View */
/* @var $model common\modules\article\models\Post */

$this->title = Article::t('article', 'Create post');
$this->params['breadcrumbs'][] = ['label' => Article::t('article', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-create">

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
        'tags' => $tags,
    ]) ?>

</div>
