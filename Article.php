<?php

namespace xolodok\article;

use yii\base\BootstrapInterface;
use yii\base\Module;
use Yii;

/**
 * Article module definition class
 */
class Article extends Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'xolodok\article\controllers';

    public $defaultLanguage;

    public $languages = [];

    public $visibleUrl = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if($this->defaultLanguage === null){
            $this->defaultLanguage = Yii::$app->sourceLanguage;
        }

        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['xolodok/article/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@vendor/xolodok/yii2-article/messages',
            'fileMap' => [
                'xolodok/article/article' => 'default.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('xolodok/article/' . $category, $message, $params, $language);
    }

    public function bootstrap($app)
    {
        // TODO: Implement bootstrap() method.
    }
}
