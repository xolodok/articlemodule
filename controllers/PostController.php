<?php

namespace xolodok\article\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use xolodok\article\models\Post;
use xolodok\article\models\PostSearch;
use xolodok\article\models\Category;
use xolodok\article\models\Image;
use xolodok\article\models\Tag;
use xolodok\article\Article;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'change-status' => [
                'class' => 'xolodok\status\StatusAction',
                'modelClass' => Post::className(),
                'messages' => [
                    'active' => [
                        Article::t('article', 'Not active'),
                        Article::t('article', 'Active')
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $categories = ArrayHelper::map(Category::find()->all(), 'id', 'title');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categories' => $categories,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Article::t('article', 'Saved successfully'));
            return $this->redirect(['index']);
        }
        else {
            $categories = ArrayHelper::map(Category::findActive($model->id), 'id', 'title');
            $tags = ArrayHelper::map(Tag::find()->all(), 'id', 'title');

            return $this->render('create', [
                'model' => $model,
                'categories' => $categories,
                'tags' => $tags,
            ]);
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Article::t('article', 'Saved successfully'));
            return $this->redirect(['index']);
        }
        else {
            $categories = ArrayHelper::map(Category::findActive($model->id), 'id', 'title');
            $tags = ArrayHelper::map(Tag::find()->all(), 'id', 'title');

            return $this->render('update', [
                'model' => $model,
                'categories' => $categories,
                'tags' => $tags,
            ]);
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImageDelete()
    {
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($image = Image::findOne(Yii::$app->request->post('key'))){
                $image->delete();

                return ['success' => true];
            }

            return ['success' => false];
        }
    }
}
