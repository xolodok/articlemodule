<?php

namespace xolodok\article\models;

use Yii;

/**
 * This is the model class for table "post_categories_lang".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $language
 * @property string $title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 */
class CategoryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_categories_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'required'],
            [['category_id'], 'integer'],
            [['language', 'title', 'meta_title', 'meta_description', 'meta_keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('article', 'ID'),
            'category_id' => Yii::t('article', 'Category ID'),
            'language' => Yii::t('article', 'Language'),
            'title' => Yii::t('article', 'Title'),
            'meta_title' => Yii::t('article', 'Meta Title'),
            'meta_description' => Yii::t('article', 'Meta Description'),
            'meta_keywords' => Yii::t('article', 'Meta Keywords'),
        ];
    }
}
