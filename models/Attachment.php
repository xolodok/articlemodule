<?php

namespace xolodok\article\models;

use Yii;

/**
 * This is the model class for table "post_attachments".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $name
 * @property string $file
 */
class Attachment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_attachments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id'], 'required'],
            [['post_id'], 'integer'],
            [['name', 'file'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('article', 'ID'),
            'post_id' => Yii::t('article', 'Post ID'),
            'name' => Yii::t('article', 'Name'),
            'file' => Yii::t('article', 'File'),
        ];
    }
}
