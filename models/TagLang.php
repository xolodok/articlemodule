<?php

namespace xolodok\article\models;

use Yii;

/**
 * This is the model class for table "post_tags_lang".
 *
 * @property integer $id
 * @property integer $tag_id
 * @property string $language
 * @property string $title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 */
class TagLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_tags_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_id'], 'required'],
            [['tag_id'], 'integer'],
            [['language', 'title', 'meta_title', 'meta_description', 'meta_keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('article', 'ID'),
            'tag_id' => Yii::t('article', 'Tag ID'),
            'language' => Yii::t('article', 'Language'),
            'title' => Yii::t('article', 'Title'),
            'meta_title' => Yii::t('article', 'Meta Title'),
            'meta_description' => Yii::t('article', 'Meta Description'),
            'meta_keywords' => Yii::t('article', 'Meta Keywords'),
        ];
    }
}
