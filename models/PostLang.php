<?php

namespace xolodok\article\models;

use Yii;

/**
 * This is the model class for table "posts_lang".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $language
 * @property string $title
 * @property string $text
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 */
class PostLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_posts_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id'], 'required'],
            [['post_id'], 'integer'],
            [['text'], 'string'],
            [['language', 'title', 'meta_title', 'meta_description', 'meta_keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('article', 'ID'),
            'post_id' => Yii::t('article', 'Post ID'),
            'language' => Yii::t('article', 'Language'),
            'title' => Yii::t('article', 'Title'),
            'text' => Yii::t('article', 'Text'),
            'meta_title' => Yii::t('article', 'Meta Title'),
            'meta_description' => Yii::t('article', 'Meta Description'),
            'meta_keywords' => Yii::t('article', 'Meta Keywords'),
        ];
    }
}
