<?php

namespace xolodok\article\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;
use omgdef\multilingual\MultilingualQuery;
use yii\web\UploadedFile;
use yii\helpers\Url;
use xolodok\fileupload\traits\ImageUpload;
use xolodok\article\Article;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $category_id
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $active
 * @property string $slug
 * @property string $image
 * @property integer $author
 * @property string $date_created
 * @property string $date_modified
 */
class Post extends \yii\db\ActiveRecord
{
    use ImageUpload;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const THUMB_WIDTH = 300;
    const DETAIL_WIDTH = 800;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_posts';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => true,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Article::getInstance()->languages,
                'dynamicLangClass' => false,
                'langClassName' => PostLang::className(),
                'defaultLanguage' => Article::getInstance()->defaultLanguage,
                'langForeignKey' => 'post_id',
                'tableName' => PostLang::tableName(),
                'attributes' => [
                    'title', 'text', 'meta_title', 'meta_description', 'meta_keywords'
                ]
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_created',
                'updatedAtAttribute' => 'date_modified',
                'value' => function(){ return date('Y-m-d H:i:s');},
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'author',
                'updatedByAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text', 'category_id'], 'required'],
            [['text'], 'string'],
            [['category_id', 'active', 'author'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
            [['title', 'meta_title', 'meta_description', 'meta_keywords', 'slug', 'image'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            ['image', 'file'],
            ['category_id', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Article::t('article', 'ID'),
            'title' => Article::t('article', 'Title'),
            'text' => Article::t('article', 'Text'),
            'category_id' => Article::t('article', 'Main category'),
            'meta_title' => Article::t('article', 'Meta title'),
            'meta_description' => Article::t('article', 'Meta description'),
            'meta_keywords' => Article::t('article', 'Meta keywords'),
            'active' => Article::t('article', 'Status'),
            'slug' => Article::t('article', 'Slug'),
            'image' => Article::t('article', 'Image'),
            'author' => Article::t('article', 'Author'),
            'date_created' => Article::t('article', 'Date created'),
            'date_modified' => Article::t('article', 'Date Modified'),
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable(PostCategory::tableName(), ['post_id' => 'id']);
    }

    public function getImages()
    {
        return $this->hasMany(Image::className(), ['post_id' => 'id']);
    }

    public function getAttachments()
    {
        return $this->hasMany(Attachment::className(), ['post_id' => 'id']);
    }

    public function getLang()
    {
        return $this->hasOne(PostLang::className(), ['post_id' => 'id'])
            ->where(['language' => substr(Yii::$app->language, 0, 2)]);
    }

    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable(PostTag::tableName(), ['post_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return (new MultilingualQuery(get_called_class()))->multilingual();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->saveImage();

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete(); // TODO: Change the autogenerated stub

        $this->deleteOldImage();
        PostTag::deleteAll(['post_id' => $this->id]);
        PostCategory::deleteAll(['post_id' => $this->id]);

        foreach ($this->images as $image){
            $this->unlink('images', $image, true);
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->saveTags();
        $this->saveCategories();
        $this->saveImages();

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    /**
     * Возвращает главную директорию для изображений
     * @return string
     */
    protected function getUploadFolder()
    {
        return '/upload/article/post';
    }

    /**
     * Сохранение изображения
     * @return void
     */
    public function saveImage()
    {
        if($image = UploadedFile::getInstance($this, 'image')){
            $folders = $this->getFolders();
            if($fileName = $this->upload($image, $folders['original'])){
                $this->deleteOldImage();
                $this->image = $fileName;
                $origFile = $folders['original'] . '/' . $fileName;
                $this->thumbnail($origFile, $folders['thumb'], self::THUMB_WIDTH);
                $this->thumbnail($origFile, $folders['detail'], self::DETAIL_WIDTH);
            }
        }
    }

    /**
     * Сохранение дополнительных изображения
     * @return void
     */
    public function saveImages()
    {
        if($images = UploadedFile::getInstancesByName('images')) {
            $folders = $this->getFolders();
            foreach ($images as $image) {
                if ($fileName = $this->upload($image, $folders['original'])) {
                    $origFile = $folders['original'] . '/' . $fileName;
                    $this->thumbnail($origFile, $folders['thumb'], self::THUMB_WIDTH);
                    $this->thumbnail($origFile, $folders['detail'], self::DETAIL_WIDTH);

                    $model = new Image();
                    $model->file = $fileName;
                    $this->link('images', $model);
                }
            }
        }
    }

    /**
     * Возвращает путь к изображению
     * @return array
     */
    public function getPreviewSrc()
    {
        return $this->image ? [$this->detailImage] : [];
    }

    /**
     * Возвращает свойство изображения
     * @return string
     */
    protected function getImageAttr()
    {
        return 'image';
    }

    public function getSelectedCategories()
    {
        $selectedIds = $this->getCategories()->select('id')->asArray()->all();
        $ids = PostCategory::find()->where(['post_id' => $this->id])->select('category_id')->asArray()->all();

        return ArrayHelper::getColumn($ids, 'category_id');
    }

    public function getSelectedTags()
    {
        $selectedIds = $this->getTags()->select('id')->asArray()->all();
        $ids = PostTag::find()->where(['post_id' => $this->id])->select('tag_id')->asArray()->all();

        return ArrayHelper::getColumn($ids, 'tag_id');
    }

    public function getPreviewImages()
    {
        $src = [];

        if(!empty($this->images)){
            foreach ($this->images as $image){
                $src[] = $image->detailImage;
            }
        }

        return $src;
    }

    public function initialPreviewConfig()
    {
        $config = [];

        if(!empty($this->images)){
            foreach ($this->images as $key => $image){
                $config[] = [
                    'caption' => "Image" . $key,
                    'width' => "120px",
                    'url' => Url::to(['/article/post/image-delete/']),
                    'key' => $image->id,
                ];
            }
        }

        return $config;
    }

    public function saveCategories()
    {
        PostCategory::deleteAll(['post_id' => $this->id]);
        if($categories = Yii::$app->request->post('categories')){
            if(is_array($categories)){
                foreach ($categories as $key => $category) {
                    if($model = Category::findOne($category)){
                        $this->link('categories', $model);
                    }
                }
            }
        }
    }

    public function saveTags()
    {
        PostTag::deleteAll(['post_id' => $this->id]);
        if($tags = Yii::$app->request->post('tags')){
            if(is_array($tags)){
                foreach ($tags as $key => $tag) {
                    if($model = Tag::findOne($tag)){
                        $this->link('tags', $model);
                    }
                }
            }
        }
    }
}
