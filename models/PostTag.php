<?php

namespace xolodok\article\models;

use Yii;

/**
 * This is the model class for table "post_to_tag".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $tag_id
 */
class PostTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_post_to_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'tag_id'], 'required'],
            [['post_id', 'tag_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('article', 'ID'),
            'post_id' => Yii::t('article', 'Post ID'),
            'tag_id' => Yii::t('article', 'Tag ID'),
        ];
    }
}
