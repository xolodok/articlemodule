<?php

use yii\db\Migration;

class m170607_131936_init extends Migration
{
    public function safeUp()
    {
        $this->createTable('article_categories', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'parent_id' => $this->integer()->notNull()->defaultValue(0),
            'meta_title' => $this->string(),
            'meta_description' => $this->string(),
            'meta_keywords' => $this->string(),
            'active' => $this->integer()->notNull()->defaultValue(0),
            'slug' => $this->string(),
            'image' => $this->string(),
            'date_created' => $this->dateTime(),
            'date_modified' => $this->dateTime(),
        ]);

        $this->createTable('article_posts', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'text' => $this->text(),
            'category_id' => $this->integer()->notNull()->defaultValue(0),
            'meta_title' => $this->string(),
            'meta_description' => $this->string(),
            'meta_keywords' => $this->string(),
            'active' => $this->integer()->notNull()->defaultValue(0),
            'slug' => $this->string(),
            'image' => $this->string(),
            'author' => $this->integer()->notNull()->defaultValue(0),
            'date_created' => $this->dateTime(),
            'date_modified' => $this->dateTime(),
        ]);

        $this->createTable('article_tags', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'meta_title' => $this->string(),
            'meta_description' => $this->string(),
            'meta_keywords' => $this->string(),
            'slug' => $this->string(),
            'date_created' => $this->dateTime(),
            'date_modified' => $this->dateTime(),
        ]);

        $this->createTable('article_post_to_tag', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('article_images', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'file' => $this->string(),
        ]);

        $this->createTable('article_attachments', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'file' => $this->string(),
        ]);

        $this->createTable('article_posts_lang', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'language' => $this->string(),
            'title' => $this->string(),
            'text' => $this->text(),
            'meta_title' => $this->string(),
            'meta_description' => $this->string(),
            'meta_keywords' => $this->string(),
        ]);

        $this->createTable('article_categories_lang', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'language' => $this->string(),
            'title' => $this->string(),
            'meta_title' => $this->string(),
            'meta_description' => $this->string(),
            'meta_keywords' => $this->string(),
        ]);

        $this->createTable('article_tags_lang', [
            'id' => $this->primaryKey(),
            'tag_id' => $this->integer()->notNull(),
            'language' => $this->string(),
            'title' => $this->string(),
            'meta_title' => $this->string(),
            'meta_description' => $this->string(),
            'meta_keywords' => $this->string(),
        ]);

        $this->createTable('article_post_to_category', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('slug', 'article_posts', ['slug'], true);
        $this->createIndex('slug', 'article_categories', ['slug'], true);
        $this->createIndex('slug', 'article_tags', ['slug'], true);
    }

    public function safeDown()
    {
        $this->dropTable('article_categories');
        $this->dropTable('article_posts');
        $this->dropTable('article_tags');
        $this->dropTable('article_images');
        $this->dropTable('article_attachments');
        $this->dropTable('article_post_to_tag');
        $this->dropTable('article_categories_lang');
        $this->dropTable('article_posts_lang');
        $this->dropTable('article_tags_lang');
    }
}
